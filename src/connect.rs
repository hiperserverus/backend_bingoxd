use std::fs;

use mysql::{Opts, OptsBuilder};
use r2d2_mysql::MysqlConnectionManager;

#[allow(non_snake_case)]
pub type MysqlPool = r2d2::Pool<MysqlConnectionManager>;

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug)]
pub struct AppConfig {
	pub cnxstr_admin	: String,
	pub cnxstr_data		: String,
	pub video_path		: String,
	pub photo_path		: String
}
impl AppConfig{
	#[allow(non_snake_case)]
	pub fn defaultFromFile() -> AppConfig {
		let cfgfile = fs::read_to_string("appconfig.json").unwrap();
		let appcfg : AppConfig = serde_json::from_str(&cfgfile).unwrap();
		appcfg
	}
}


#[derive(Debug)]
pub struct Ghook{
	pub admin_pool	: MysqlPool,
	pub data_pool	: MysqlPool
}
impl Ghook{
	pub fn new(admin_cnxstr: String, data_cnxstr: String) -> Ghook {
		Ghook{
			admin_pool	: Ghook::getPool(admin_cnxstr),
			data_pool	: Ghook::getPool(data_cnxstr)
		}
	}

	#[allow(non_snake_case)]
	pub fn getPool(cnxString: String) -> MysqlPool{
		let opts = Opts::from_url(&cnxString).unwrap();
		let builder = OptsBuilder::from_opts(opts);
		let manager = r2d2_mysql::MysqlConnectionManager::new(builder);
		r2d2::Pool::builder().build(manager).expect("Failed to create pool!")
	}
}
