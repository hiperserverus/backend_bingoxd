//use std::str::FromStr;
use mysql::{params,Row};

use rocket::{get,State};
use rocket_contrib::json::{Json,JsonValue};

use crate::models::*;
use crate::connect::*;
use crate::modelsdao::*;
use crate::utilmodels::*;

//use serde::{Deserialize, Serialize};
//use serde_json::{Result, Value};

//default value for string column
fn strdef(anyrow: &Row, colindex: usize) -> String{
	let str_val = anyrow.get(colindex).unwrap();
	let strraw: String;
	match str_val{
		Some(v) => strraw = v,
		None 	=> strraw = "".to_string()
	}
	strraw
}

//---- Login User

#[allow(non_snake_case)]
#[post("/loginuser",format="json",data="<any>")]
pub fn user_login(mut any: Json<LoginUser>, ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!("usuario = '{}' AND clave = '{}' AND estado = 'ACTIVO'", any.usuario, any.clave);
	let mut credentials : Vec<LoginUserTemp> = Vec::new();
	credentials = LoginUsersTemp::filter(&ghook.data_pool, wheresql);
	if(credentials.len() == 0){
		json!({
			"code"          : "400",
			"message"       : "Usuario o contraseña incorrecta",
			"obj"           : ""
		})
	}else {

		let credential = credentials[0].clone();
		let mut whereuserssql = format!("usuario = '{}' AND clave = '{}' AND estado = 'ACTIVO'", credential.usuario, credential.clave); 
		let mut admUsers;
		let mut sucursales;
		let mut vendedores;
		let mut terminales;
		let mut json;
		if(credential.tipo == "ADM".to_string()){
			admUsers = AdminUsers::filter(&ghook.data_pool, whereuserssql);
			json = json!({
				"code"          : "400",
				"message"       : "Correcto",
				"obj"           : admUsers
			})
		}else {
			if(credential.tipo == "SUC".to_string()){
				sucursales = Sucursales::filter(&ghook.data_pool, whereuserssql);
				json = json!({
					"code"          : "400",
					"message"       : "Correcto",
					"obj"           : sucursales
				})
			}else{
				if(credential.tipo == "VEN".to_string()){
					vendedores = Vendedores::filter(&ghook.data_pool, whereuserssql);
					json = json!({
						"code"          : "400",
						"message"       : "Correcto",
						"obj"           : vendedores
					})
				}else{
					terminales = Terminales::filter(&ghook.data_pool, whereuserssql);
					json = json!({
						"code"          : "400",
						"message"       : "Correcto",
						"obj"           : terminales
					})
				}
			}
		}
		
		json
	}
}

#[allow(non_snake_case)]
#[post("/addadmuser",format="json",data="<any>")]
pub fn admuser_add(mut any: Json<AdminUser>, ghook:State<Ghook>) -> JsonValue{
	let mut new_user = &any.clone();
	let id_user = AdminUsers::create(&ghook.data_pool, &any);
	let  wheresql = format!(" id_auser = {}", id_user);
	let users = AdminUsers::filter(&ghook.data_pool, wheresql);
	let mut  user = users[0].clone();

	let mut level: String = "".to_owned();
	let separator =  ",";
	let value: &str = "0";
	let mut id_global: &str = "0";
	let mut id_grupo: &str = "0";
	let mut id_administrador: &str = "0";
	let mut  id_sucursal: &str = "0";

	let l: &i32 = &new_user.id_auser;
	let o: String = l.to_string();
	id_global = &o;


	match new_user.tipo.as_str() {
		"GLOBAL" => {

			level = getLevel(id_global, separator, id_grupo, id_administrador, id_sucursal);

			println!("VALOR DE ID_GLOBAL: {}", id_global);
			println!("VALOR DE ID_GRUPO: {}", id_grupo);

			let  sql = format!(" id_auser = {}", id_user);

			AdminUsers::edit_nivel(&ghook.data_pool, sql, level);


		
		
		},


		"GRUPO" => {



			let y: &u64 = &id_user;
			let m: String = y.to_string();
			id_grupo = &m;

			level = getLevel(id_global, separator, id_grupo, id_administrador, id_sucursal);

			println!("VALOR DE ID_GLOBAL: {}", id_global);
			println!("VALOR DE ID_GRUPO: {}", id_grupo);

			let  sql = format!(" id_auser = {}", id_user);

			AdminUsers::edit_nivel(&ghook.data_pool, sql, level);
		
		
		},

		"ADMINISTRADOR" => {

			let mut array: &str = &new_user.nivel;
			let mut vc: Vec<i32> = AdminUsers::getLevelsToArr(array);
			let mut x: &i32 = &vc[1];
			let mut s: String = x.to_string();

			id_grupo = &s;

			let y: &u64 = &id_user;
			let m: String = y.to_string();

			id_administrador = &m;

			level = getLevel(id_global, separator, id_grupo, id_administrador, id_sucursal);


			let  sql = format!(" id_auser = {}", id_user);

			AdminUsers::edit_nivel(&ghook.data_pool, sql, level);
		
		
		},
		_ => {
			
			
		
		
		}
	};

	json!({
		"status"          : "200",
		"message"       : "Success cretation¡¡¡",
		"Created"           : user
	})


}

#[allow(non_snake_case)]
#[post("/editadmuser",format="json",data="<any>")]
pub fn admuser_edit(mut any: Json<AdminUser>, ghook:State<Ghook>){
	AdminUsers::edit(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[get("/filteradmuser",format="json",data="<any>")]
pub fn admusers_filter(mut any: Json<WhereStr>, ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!(" {}", any.wherestr.to_owned());
	let users = AdminUsers::filter(&ghook.data_pool, wheresql);
	let user = users[0].clone();

	json!({
		"code"          : "400",
		"message"       : "Success admin users",
		"obj"           : user
	})

}



#[allow(non_snake_case)]
#[post("/addsucursal",format="json",data="<any>")]
pub fn sucursal_add(mut any: Json<Sucursal>, ghook:State<Ghook>) -> JsonValue{
	let id_suc = Sucursales::create(&ghook.data_pool, &any);
	// // let wheresql = format!(" id_suc = {}", id_suc);
	// ///////////////
	// let mut id_auser: &str = "0";
	// let mut new_user = &any.clone();
	// let mut l: &i32 = &new_user.id_auser;
	// let mut o: String = l.to_string();
	// id_auser = &o;
	// ///////////////

	// let  wheresql = format!(" id_auser = {}", id_auser);
	// let users = AdminUsers::filter(&ghook.data_pool, wheresql);
	// let mut user = users[0].clone();

	// ///////////

	// let mut lv: &str = &user.nivel;
	// let mut vc: Vec<i32> = AdminUsers::getLevelsToArr(lv);

	// let mut vctr: Vec<i32> = AdminUsers::getLevelId(vc, id_suc as i32);

	// let level: String = AdminUsers::getLevelsToString(vctr);


	// let  wheresql = format!(" id_auser = {}", id_auser);
	
	// AdminUsers::edit_nivel(&ghook.data_pool, wheresql, level);

	json!({
		"code"          : "200",
		"message"       : "Success create Sucursal",
	})
}

#[allow(non_snake_case)]
#[post("/editsucursal",format="json",data="<any>")]
pub fn sucursal_edit(mut any: Json<Sucursal>, ghook:State<Ghook>){
	Sucursales::edit(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[get("/filtergroups/<id>")]
pub fn groups_filter(mut id: String, ghook:State<Ghook>) -> JsonValue{
	let global_id = id;
	let groups = AdminUsers::filter_groups(&ghook.data_pool, global_id);


	json!({
		"status"          : "200",
		"message"       : "Success groups",
		"groups"           : groups
	})
}

#[allow(non_snake_case)]
#[get("/filteradmins")]
pub fn filter_admins_all(ghook:State<Ghook>) -> JsonValue{
	let query = format!(";"); 
	let admins = AdminUsers::filter_administrators(&ghook.data_pool, query);


	json!({
		"status"          : "200",
		"message"       : "Success admins",
		"admins"           : admins
	})
}

#[allow(non_snake_case)]
#[get("/filteradmins/<id>")]
pub fn filter_admins_id(mut id: String, ghook:State<Ghook>) -> JsonValue{
	let group_id = id;
	let query = format!(" AND  nivel LIKE '%,{},%'", group_id); 
	let admins = AdminUsers::filter_administrators(&ghook.data_pool, query);


	json!({
		"status"          : "200",
		"message"       : "Success admins for group",
		"admins"           : admins
	})
}

#[allow(non_snake_case)]
#[get("/filtersucursals")]
pub fn filter_suc_all(ghook:State<Ghook>) -> JsonValue{
	let query = format!(";"); 
	let sucursales = Sucursales::filter(&ghook.data_pool, query);


	json!({
		"status"          : "200",
		"message"       : "Success admins",
		"sucursales"           : sucursales
	})
}

#[allow(non_snake_case)]
#[get("/filtersucursales/<id>")]
pub fn sucursales_filter(mut id: String, ghook:State<Ghook>) -> JsonValue{
	let admin_id = id;
	let query = format!(" Where id_auser = {}", admin_id); 
	let sucursales = Sucursales::filter(&ghook.data_pool, query);


	json!({
		"status"          : "200",
		"message"       : "Success sucursals filter",
		"sucursales"           : sucursales
	})
}

// #[allow(non_snake_case)]
// #[get("/filtersucursales",format="json",data="<any>")]
// pub fn sucursales_filter(mut any: Json<WhereStr>, ghook:State<Ghook>) -> JsonValue{
// 	let wheresql = format!(" {}", any.wherestr.to_owned());
// 	let users = Sucursales::filter(&ghook.data_pool, wheresql);
// 	let user = users[0].clone();

// 	json!({
// 		"code"          : "400",
// 		"message"       : "Success sucursales",
// 		"obj"           : user
// 	})
// }

#[allow(non_snake_case)]
#[post("/addvendedor",format="json",data="<any>")]
pub fn vendedor_add(mut any: Json<Vendedor>, ghook:State<Ghook>) -> JsonValue{
	let id_ven = Vendedores::create(&ghook.data_pool, &any);
	let wheresql = format!(" id_ven = {}", id_ven);
	let users = Vendedores::filter(&ghook.data_pool, wheresql);
	let user = users[0].clone();

	json!({
		"code"          : "400",
		"message"       : "Success sucursales",
		"obj"           : user
	})
}

#[allow(non_snake_case)]
#[post("/editvendedor",format="json",data="<any>")]
pub fn vendedor_edit(mut any: Json<Vendedor>, ghook:State<Ghook>){
	Vendedores::edit(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[get("/filtervendedores",format="json",data="<any>")]
pub fn vendedores_filter(mut any: Json<WhereStr>, ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!(" {}", any.wherestr.to_owned());
	let users = Vendedores::filter(&ghook.data_pool, wheresql);
	let user = users[0].clone();

	json!({
		"code"          : "400",
		"message"       : "Success sucursales",
		"obj"           : user
	})
}


#[allow(non_snake_case)]
#[post("/addterminal",format="json",data="<any>")]
pub fn terminal_add(mut any: Json<Terminal>, ghook:State<Ghook>) -> JsonValue{
	let id_ter = Terminales::create(&ghook.data_pool, &any);

	json!({
		"code"          : "200",
		"message"       : "Success terminales",
	})
}

#[allow(non_snake_case)]
#[post("/editterminal",format="json",data="<any>")]
pub fn terminal_edit(mut any: Json<Terminal>, ghook:State<Ghook>){
	Terminales::edit(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[get("/filterterminales")]
pub fn terminales_filter( ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!("{}", "where mid(nombre,1,4) != 'BOT~'");
	let terminals = Terminales::filter(&ghook.data_pool, wheresql);

	json!({
		"code"          : "400",
		"message"       : "Success terminals",
		"terminals"           : terminals
	})
}


#[allow(non_snake_case)]
#[post("/addbot",format="json",data="<any>")]
pub fn bot_add(mut any: Json<Terminal>, ghook:State<Ghook>) -> JsonValue{
	let id_bot = Terminales::create(&ghook.data_pool, &any);

	json!({
		"code"          : "200",
		"message"       : "Success bots",
	})
}

#[allow(non_snake_case)]
#[post("/editbot",format="json",data="<any>")]
pub fn bot_edit(mut any: Json<Terminal>, ghook:State<Ghook>){
	Terminales::edit(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[get("/filterbots")]
pub fn bots_filter( ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!("where mid(nombre,1,4) = 'BOT~'");
	let bots = Terminales::filter(&ghook.data_pool, wheresql);

	json!({
		"code"          : "200",
		"message"       : "Success countrys",
		"bots"           : bots
	})
}

#[allow(non_snake_case)]
#[post("/addcountry",format="json",data="<any>")]
pub fn country_add(mut any: Json<Pais>, ghook:State<Ghook>) -> JsonValue{
	let id_country = Paises::create(&ghook.data_pool, &any);

	json!({
		"code"          : "200",
		"message"       : "Success add country",
	})
}

#[allow(non_snake_case)]
#[post("/editcountry",format="json",data="<any>")]
pub fn country_edit(mut any: Json<Pais>, ghook:State<Ghook>) -> JsonValue{
	let id_country = Paises::edit(&ghook.data_pool, &any);

	json!({
		"code"          : "200",
		"message"       : "Success edit country",
	})
}

#[allow(non_snake_case)]
#[get("/filtercountrys")]
pub fn countrys_filter( ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!(";");
	let countrys = Paises::filter(&ghook.data_pool, wheresql);

	json!({
		"code"          : "200",
		"message"       : "Success countrys",
		"countrys"           : countrys
	})
}

fn getLevel(id_global: &str, separator: &str, id_grupo: &str, id_administrador: &str, id_sucursal: &str) -> String {
	let mut level: String = "".to_string();
	level.push_str(&id_global);
	level.push_str(separator);
	level.push_str(id_grupo);
	level.push_str(separator);
	level.push_str(id_administrador);
	level.push_str(separator);
	level.push_str(id_sucursal);

	level
}

#[allow(non_snake_case)]
#[post("/editticket",format="json",data="<any>")]
pub fn ticket_edit(mut any: Json<Ticket>, ghook:State<Ghook>) -> JsonValue{
	//let id_country = Paises::edit(&ghook.data_pool, &any);
	Tickets::edit(&ghook.data_pool, &any);
	json!({
		"code"          : "200",
		"message"       : "Success edit ticket"
	})
}

#[allow(non_snake_case)]
#[post("/filtertickets",format="json",data="<any>")]
pub fn tickets_filter(mut any: Json<WhereStr>, ghook:State<Ghook>) -> JsonValue{
	let wheresql = format!(" {}", any.wherestr.to_owned());
	let tickets = Tickets::filter(&ghook.data_pool, wheresql);

	json!({
		"code"          : "200",
		"message"       : "Success tickets",
		"tickets"          : tickets
	})
}

#[allow(non_snake_case)]
#[post("/cancelticket",format="json",data="<any>")]
pub fn ticket_cancel(mut any: Json<Ticket>, ghook:State<Ghook>) -> JsonValue{
	//let id_country = Paises::edit(&ghook.data_pool, &any);
	Tickets::edit(&ghook.data_pool, &any);
	CsApos::kill(&ghook.data_pool, any.id_ticket);

	let wherePozo = format!("tipo = 'LINEAL' AND ganadores = 0 AND idpais = {}", any.id_pais);
	let pozos = Jackpots::filter(&ghook.data_pool, wherePozo);
	let mut pozo = pozos[0].clone();
	let minusPozo = any.cantidad_apostada * pozo.porcentaje;
	pozo.total = pozo.total - minusPozo;
	Jackpots::edit(&ghook.data_pool, &pozo);

	let whereJackpot = format!("tipo = 'ESPECIAL' AND ganadores = 0 AND idpais = {}", any.id_pais);
	let jackpots = Jackpots::filter(&ghook.data_pool, whereJackpot);
	let mut jackpot = jackpots[0].clone();
	let minusJackpot = any.cantidad_apostada * jackpot.porcentaje;
	jackpot.total = jackpot.total - minusJackpot;
	Jackpots::edit(&ghook.data_pool, &jackpot);
	
	json!({
		"code"          : "200",
		"message"       : "Success cancel ticket"
	})
}

#[allow(non_snake_case)]
#[post("/printticket",format="json",data="<any>")]
pub fn ticket_print(mut any: Json<Ticket>, ghook:State<Ghook>) -> JsonValue{
	let mut ticketsImp: Vec<TicketImp> = Vec::new();
	let tickets = Tickets::filter_ticketimp(&ghook.data_pool, any.id_sucursal, any.tipo_usu.to_owned(), any.id_ticket);
	let mut ticketI = tickets[0].clone();
	let mut newTicket = Tickets::getBarcode(ticketI).to_owned();
	ticketsImp.push(newTicket);
	
	json!({
		"code"          : "200",
		"message"       : "Success ticket imp",
		"ticketImp"		: ticketsImp
	})
}



/* #[allow(non_snake_case)]
#[post("/addany",format="json",data="<any>")]
pub fn any_add(mut any: Json<Carton>, ghook:State<Ghook>){
	Cartones::create(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[post("/additems",format="json",data="<items>")]
pub fn items_add(mut items: Json<CartonesJson>, ghook:State<Ghook>){
	let cartones : Vec<Carton> = Cartones::json_to_struc(items);
	Cartones::create_batch(&ghook.data_pool, &cartones);
}

#[allow(non_snake_case)]
#[post("/editany",format="json",data="<any>")]
pub fn any_edit(mut any: Json<Carton>, ghook:State<Ghook>){
	Cartones::edit(&ghook.data_pool, &any);
}

#[allow(non_snake_case)]
#[post("/edititems",format="json",data="<items>")]
pub fn items_edit(mut items: Json<CartonesJson>, ghook:State<Ghook>){
	let cartones : Vec<Carton> = Cartones::json_to_struc(items);
	Cartones::edit_batch(&ghook.data_pool, &cartones);
}

#[allow(non_snake_case)]
#[get("/filteritem",format="json",data="<any>")]
pub fn items_filter(mut any: Json<Carton>, ghook:State<Ghook>){
	let wheresql = format!(" id_cart = {}", any.id_cart);
	Cartones::filter(&ghook.data_pool, wheresql);
}

#[allow(non_snake_case)]
#[post("/deleteany",format="json",data="<any>")]
pub fn any_delete(mut any: Json<Carton>, ghook:State<Ghook>){
	Cartones::kill(&ghook.data_pool, any.id_cart);
} */