#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

extern crate r2d2;
extern crate r2d2_mysql;

extern crate mysql;

extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate serde_json;


extern crate rocket_cors;

use rocket::{routes};
use rocket::http::Method;
use rocket_cors::{AllowedHeaders, AllowedOrigins};

pub mod connect;
pub mod models;
pub mod endpoints;
pub mod modelsdao;
pub mod utilmodels;

use connect::{AppConfig,Ghook};

fn main() {
    let appcfg = AppConfig::defaultFromFile();
	let (allowed_origins, _failed_origins) = AllowedOrigins::some(&["http://localhost:4401", "http://localhost:8081","http://localhost:8080"]);
	//let (allowed_origins, _failed_origins) = AllowedOrigins::some(&["*","*"]);

    let options = rocket_cors::Cors {
        allowed_origins: allowed_origins,
        allowed_methods: vec![Method::Get,Method::Post,Method::Put,Method::Delete,Method::Options].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::all(),
        allow_credentials: true,
        ..Default::default()
    };

    rocket::ignite()
    .manage(Ghook::new(appcfg.cnxstr_admin, appcfg.cnxstr_data))
    .mount("/bingoretailapi",rocket_cors::catch_all_options_routes())
    .mount("/bingoretailapi",routes![endpoints::user_login, 
    endpoints::admuser_add, endpoints::admuser_edit, endpoints::admusers_filter, endpoints::groups_filter, endpoints::filter_admins_all,endpoints::filter_admins_id,
    endpoints::sucursal_add, endpoints::sucursal_edit, endpoints::sucursales_filter, endpoints::filter_suc_all,
    endpoints::vendedor_add, endpoints::vendedor_edit, endpoints::vendedores_filter,
    endpoints::terminal_add, endpoints::terminal_edit, endpoints::terminales_filter,
    endpoints::bot_add, endpoints::bot_edit,endpoints::bots_filter,
    endpoints::country_add, endpoints::country_edit, endpoints::countrys_filter,
    endpoints::ticket_edit, endpoints::tickets_filter, endpoints::ticket_cancel,
    endpoints::ticket_print,
    ])
    .manage(options.clone())
    .attach(options.clone())//important for cors
    .launch();
}