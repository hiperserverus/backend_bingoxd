use std::fmt;

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct AdminUser {
    pub id_auser: i32,
	pub nombre: String,
	pub usuario: String,
    pub clave: String,
    pub fecha: String, 
    pub fecha_login: String,
    pub sesion: String,
    pub sesion_tok: String,
    pub pais: String,
    pub estado: String,
    pub tipo: String,
    pub cr_creditos: i32,
    pub cr_creditos_usa: i32,
    pub cr_fecha_mod: String,
    pub nivel: String
}

impl Default for AdminUser {
	fn default() -> AdminUser {
		AdminUser{
            id_auser: 0,
            nombre: "".to_string(),
            usuario: "".to_string(),
            clave: "".to_string(),
            fecha: "".to_string(), 
            fecha_login: "".to_string(),
            sesion: "".to_string(),
            sesion_tok: "".to_string(),
            pais: "".to_string(),
            estado: "".to_string(),
            tipo: "".to_string(), 
            cr_creditos: 0,
            cr_creditos_usa: 0,
            cr_fecha_mod: "".to_string(),
            nivel: "".to_string()
		}
	}
}

impl fmt::Display for AdminUser {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_auser:{}, nombre:{}, usuario:{}, clave:{}, fecha:{}, fecha_login:{}, sesion:{}, sesion_tok:{}, pais:{}, estado:{}, tipo:{}, cr_creditos:{}, cr_creditos_usa:{}, cr_fecha_mod:{}, nivel:{}",
        self.id_auser, self.nombre, self.usuario, self.clave, self.fecha, self.fecha_login, self.sesion, self.sesion_tok, self.pais, self.estado, self.tipo, self.cr_creditos, self.cr_creditos_usa, self.cr_fecha_mod, self.nivel)
    }
}


#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct AdminUsersJson {
	pub adm_users: Vec<AdminUser>
}

impl Default for AdminUsersJson {
	fn default() -> AdminUsersJson {
		AdminUsersJson{
			adm_users: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct CApos {
    pub id_capos: i32,
	pub id_partida: i32,
	pub carton: String,
	pub estado: String,
	pub	id_ticket: i32,
	pub id_usuario: i32,
	pub id_pais: i32,
	pub moneda: String,
	pub compat: String,
	pub tipo_usu: String
}

impl Default for CApos {
	fn default() -> CApos {
		CApos{
            id_capos: 0,
			id_partida: 0,
			carton: "".to_string(),
			estado: "".to_string(),
			id_ticket: 0,
			id_usuario: 0,
			id_pais: 0,
			moneda: "".to_string(),
			compat: "".to_string(),
			tipo_usu: "".to_string()
		}
	}
}

impl fmt::Display for CApos {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_capos:{}, id_partida:{}, carton:{}, estado:{}, id_ticket:{}, id_usuario:{}, id_pais:{}, moneda:{}, compat:{}, tipo_usu:{}",
        self.id_capos, self.id_partida, self.carton, self.estado, self.id_ticket, self.id_usuario, self.id_pais, self.moneda, self.compat, self.tipo_usu)
    }
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct CsAposJson {
	pub csapos: Vec<CApos>
}

impl Default for CsAposJson {
	fn default() -> CsAposJson {
		CsAposJson{
			csapos: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Carton {
	pub id_cart	:i32,
	pub carton	: String
}

impl fmt::Display for Carton {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f,"id_cart:{}, carton:{}",
        self.id_cart,self.carton)
    }
}

impl Default for Carton {
	fn default() -> Carton {
		Carton{
			id_cart: 0,
			carton: "".to_string()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct CartonesJson {
	pub cartones: Vec<Carton>
}

impl Default for CartonesJson {
	fn default() -> CartonesJson {
		CartonesJson{
			cartones: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Ganancia {
	pub id_gan		:i32,
	pub id_usuario	: i32,
	pub id_padre		: i32,
	pub porcentaje	: f32,
	pub porcentaje_padre	: f32,
	pub fecha				: String,
	pub fecha_mod		: String
}

impl fmt::Display for Ganancia {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_gan:{}, id_usuario:{}, id_padre:{}, porcentaje:{}, porcentaje_padre:{}, fecha:{}, fecha_mod:{}",
        self.id_gan,self.id_usuario,self.id_padre,self.porcentaje,self.porcentaje_padre,self.fecha,self.fecha_mod)
    }
}

impl Default for Ganancia {
	fn default() -> Ganancia {
		Ganancia{
			id_gan: 0,
			id_usuario	: 0,
			id_padre		: 0,
			porcentaje	: 0.0f32,
			porcentaje_padre	: 0.0f32,
			fecha				: "".to_string(),
			fecha_mod		: "".to_string()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct GananciasJson {
	pub ganancias: Vec<Ganancia>
}

impl Default for GananciasJson {
	fn default() -> GananciasJson {
		GananciasJson{
			ganancias: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Jackpot {
	pub id_jack: i32,
	pub tipo: String,
	pub fecha_creado: String,
	pub fecha_cobro: String,
	pub porcentaje: f32,
	pub sorteos_acumulados: i32, 
	pub contribucion: f32,
	pub ganadores: i32,
	pub total: f32,
	pub idpais: i32
}

impl Default for Jackpot {
	fn default() -> Jackpot {
		Jackpot{
			id_jack: 0,
			tipo: "".to_string(),
			fecha_creado: "".to_string(),
			fecha_cobro: "".to_string(),
			porcentaje: 0.00,
			sorteos_acumulados: 0, 
			contribucion: 0.00,
			ganadores: 0,
			total: 0.00,
			idpais: 0
		}
	}
}

impl fmt::Display for Jackpot {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_jack:{}, tipo:{}, fecha_creado:{}, fecha_cobro:{}, porcentaje:{}, sorteos_acumulados:{}, contribucion:{}, ganadores:{}, total:{}, idpais:{}",
        self.id_jack, self.tipo, self.fecha_creado, self.fecha_cobro, self.porcentaje, self.sorteos_acumulados, self.contribucion, self.ganadores, self.total, self.idpais)
    }
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct JackpotsJson {
	pub jackpots: Vec<Jackpot>
}

impl Default for JackpotsJson {
	fn default() -> JackpotsJson {
		JackpotsJson{
			jackpots: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Pais {
	pub id_pais: i32,
	pub nombre: String,
	pub codigo: String,
	pub simbolo: String,
	pub valor_carton: f32,
	pub fijo_pozo: f32, 
	pub fijo_jackpot: f32,
	pub porcentaje_pozo: f32,
	pub porcentaje_jackpot: f32,
    pub porcentaje_cliente: f32,
    pub porcentaje_vendedor: f32
}

impl Default for Pais {
	fn default() -> Pais {
		Pais{
            id_pais: 0,
            nombre: "".to_string(),
            codigo: "".to_string(),
            simbolo: "".to_string(),
            valor_carton: 0.00,
            fijo_pozo: 0.00, 
            fijo_jackpot: 0.00,
            porcentaje_pozo: 0.00,
            porcentaje_jackpot: 0.00,
            porcentaje_cliente: 0.00,
            porcentaje_vendedor: 0.00
		}
	}
}

impl fmt::Display for Pais {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_pais: {}, nombre: {}, codigo: {}, simbolo: {}, valor_carton: {}, fijo_pozo: {}, fijo_jackpot: {}, porcentaje_pozo: {},
        porcentaje_jackpot: {}, porcentaje_cliente: {}, porcentaje_vendedor: {}", self.id_pais, self.nombre, self.codigo, self.simbolo, 
        self.valor_carton, self.fijo_pozo, self.fijo_jackpot, self.porcentaje_pozo, self.porcentaje_jackpot, self.porcentaje_cliente, self.porcentaje_vendedor)
    }
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct PaisesJson {
	pub paises: Vec<Pais>
}

impl Default for PaisesJson {
	fn default() -> PaisesJson {
		PaisesJson{
			paises: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Partida {
	pub id_part	: i32,
	pub fecha 	: String,
	pub estado	: String,
	pub tipo		: String,
	pub bolitas_sorteo 	: String
}

impl fmt::Display for Partida {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
	    write!(f,"id_part:{}, fecha:{}, estado:{}, tipo:{}, bolitas_sorteo:{}",
        self.id_part,self.fecha,self.estado,self.tipo,self.bolitas_sorteo)
    }
}

impl Default for Partida {
	fn default() -> Partida {
		Partida{
			id_part: 0,
			fecha: "".to_owned(),
			estado: "".to_owned(),
			tipo: "".to_owned(),
            bolitas_sorteo: "".to_owned()
        }
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct PartidasJson {
	pub partidas: Vec<Partida>
}

impl Default for PartidasJson {
	fn default() -> PartidasJson {
		PartidasJson{
			partidas: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Sucursal {
    pub id_suc: i32,
	pub id_auser: i32,
	pub nombre: String,
	pub usuario: String,
    pub clave: String,
    pub fecha: String, 
    pub fecha_login: String,
    pub sesion: String,
    pub sesion_tok: String,
    pub pais: String,
    pub estado: String,
    pub direccion: String,
    pub provincia: String,
    pub cr_creditos: i32,
    pub cr_creditos_usa: i32,
    pub cr_fecha_mod: String
}

impl Default for Sucursal {
	fn default() -> Sucursal {
		Sucursal{
            id_suc: 0,
            id_auser: 0,
            nombre: "".to_string(),
            usuario: "".to_string(),
            clave: "".to_string(),
            fecha: "".to_string(), 
            fecha_login: "".to_string(),
            sesion: "".to_string(),
            sesion_tok: "".to_string(),
            pais: "".to_string(),
            estado: "".to_string(),
            direccion: "".to_string(), 
            provincia: "".to_string(), 
            cr_creditos: 0,
            cr_creditos_usa: 0,
            cr_fecha_mod: "".to_string()
		}
	}
}

impl fmt::Display for Sucursal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_suc:{}, id_auser:{}, nombre:{}, usuario:{}, clave:{}, fecha:{}, fecha_login:{}, sesion:{}, sesion_tok:{}, pais:{}, estado:{}, direccion:{}, provincia:{}, cr_creditos:{}, cr_creditos_usa:{}, cr_fecha_mod:{}",
        self.id_suc, self.id_auser, self.nombre, self.usuario, self.clave, self.fecha, self.fecha_login, self.sesion, self.sesion_tok, self.pais, self.estado, self.direccion, self.provincia, self.cr_creditos, self.cr_creditos_usa, self.cr_fecha_mod)
    }
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct SucursalesJson {
	pub sucursales: Vec<Sucursal>
}

impl Default for SucursalesJson {
	fn default() -> SucursalesJson {
		SucursalesJson{
			sucursales: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Terminal {
    pub id_ter: i32,
    pub id_suc: i32,
	pub nombre: String,
	pub usuario: String,
    pub clave: String,
    pub fecha: String, 
    pub fecha_login: String,
    pub sesion: String,
    pub sesion_tok: String,
    pub pais: String,
    pub estado: String,
    pub cr_creditos: i32,
    pub cr_creditos_usa: i32,
    pub cr_fecha_mod: String
}

impl Default for Terminal {
	fn default() -> Terminal {
		Terminal{
            id_ter: 0,
            id_suc: 0,
            nombre: "".to_string(),
            usuario: "".to_string(),
            clave: "".to_string(),
            fecha: "".to_string(), 
            fecha_login: "".to_string(),
            sesion: "".to_string(),
            sesion_tok: "".to_string(),
            pais: "".to_string(),
            estado: "".to_string(),
            cr_creditos: 0,
            cr_creditos_usa: 0,
            cr_fecha_mod: "".to_string()
		}
	}
}

impl fmt::Display for Terminal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_ter:{}, id_suc:{}, nombre:{}, usuario:{}, clave:{}, fecha:{}, fecha_login:{}, sesion:{}, sesion_tok:{}, pais:{}, estado:{}, cr_creditos:{}, cr_creditos_usa:{}, cr_fecha_mod:{}",
        self.id_ter, self.id_suc, self.nombre, self.usuario, self.clave, self.fecha, self.fecha_login, self.sesion, self.sesion_tok, self.pais, self.estado, self.cr_creditos, self.cr_creditos_usa, self.cr_fecha_mod)
    }
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct TerminalesJson {
	pub terminales: Vec<Terminal>
}

impl Default for TerminalesJson {
	fn default() -> TerminalesJson {
		TerminalesJson{
			terminales: Vec::new()
		}
	}
}


#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Ticket {
	pub	id_ticket: i32,
    pub modalidad: String,
    pub cantidad_apostada: f32,
    pub ganancia: f32, 
    pub pin: i32,
    pub estado: String, 
    pub ticket_sucursal: i32,
    pub id_sucursal: i32,
    pub id_carton: i32,
    pub carton: String,
    pub id_partida: i32,
    pub id_usuario: i32,
    pub tipo_bingo: String,
    pub moneda: String,
    pub id_pais: i32,
    pub compat: String,
	pub fecha_emision: String,
    pub fecha_cobro: String,
    pub estfecha: String,
    pub estobs: String,
	pub pagado: i32,
	pub tipo_usu: String

}

impl Default for Ticket {
	fn default() -> Ticket {
		Ticket{
			id_ticket: 0,
            modalidad: "".to_string(),
            cantidad_apostada: 0.00,
            ganancia: 0.00,
            pin: 0,
            estado: "".to_string(),
            ticket_sucursal: 0,
            id_sucursal: 0,
            id_carton: 0,
            carton: "".to_string(),
            id_partida: 0,
            id_usuario: 0,
            tipo_bingo: "".to_string(),
            moneda: "".to_string(),
            id_pais: 0,
            compat: "".to_string(),
			fecha_emision: "".to_string(),
            fecha_cobro: "".to_string(),
            estfecha: "".to_string(),
            estobs: "".to_string(),
			pagado: 0,
			tipo_usu: "".to_string()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct TicketsJson {
	pub tickets: Vec<Ticket>
}

impl Default for TicketsJson {
	fn default() -> TicketsJson {
		TicketsJson{
			tickets: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct TicketImp {
	pub id_ticket: i32,
	pub ticket0: String, 
	pub pin: i32,
	pub code: String,
	pub ticket_sucursal: i32,
	pub id_carton: i32,
	pub carton: String,
	pub sucursal: String,
	pub terminal: String,
	pub id_partida: i32,
	pub simbolo: String,
	pub monto: f32,
	pub fecha_emision: String
}

impl Default for TicketImp {
	fn default() -> TicketImp {
		TicketImp{
			id_ticket: 0,
			ticket0: "".to_string(), 
			pin: 0,
			code: "".to_string(),
			ticket_sucursal: 0,
			id_carton: 0,
			carton: "".to_string(),
			sucursal: "".to_string(),
			terminal: "".to_string(),
			id_partida: 0,
			simbolo: "".to_string(),
			monto: 0.00,
			fecha_emision: "".to_string()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct TicketsImpJson {
	pub tickets: Vec<TicketImp>
}

impl Default for TicketsImpJson {
	fn default() -> TicketsImpJson {
		TicketsImpJson{
			tickets: Vec::new()
		}
	}
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Vendedor {
    pub id_ven: i32,
    pub id_suc: i32,
	pub nombre: String,
	pub usuario: String,
    pub clave: String,
    pub fecha: String, 
    pub fecha_login: String,
    pub sesion: String,
    pub sesion_tok: String,
    pub pais: String,
    pub estado: String,
    pub cr_creditos: i32,
    pub cr_creditos_usa: i32,
    pub cr_fecha_mod: String
}

impl Default for Vendedor {
	fn default() -> Vendedor {
		Vendedor{
            id_ven: 0,
            id_suc: 0,
            nombre: "".to_string(),
            usuario: "".to_string(),
            clave: "".to_string(),
            fecha: "".to_string(), 
            fecha_login: "".to_string(),
            sesion: "".to_string(),
            sesion_tok: "".to_string(),
            pais: "".to_string(),
            estado: "".to_string(),
            cr_creditos: 0,
            cr_creditos_usa: 0,
            cr_fecha_mod: "".to_string()
		}
	}
}

impl fmt::Display for Vendedor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_ven:{}, id_suc:{}, nombre:{}, usuario:{}, clave:{}, fecha:{}, fecha_login:{}, sesion:{}, sesion_tok:{}, pais:{}, estado:{}, cr_creditos:{}, cr_creditos_usa:{}, cr_fecha_mod:{}",
        self.id_ven, self.id_suc, self.nombre, self.usuario, self.clave, self.fecha, self.fecha_login, self.sesion, self.sesion_tok, self.pais, self.estado, self.cr_creditos, self.cr_creditos_usa, self.cr_fecha_mod)
    }
}

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct VendedoresJson {
	pub vendedores: Vec<Vendedor>
}

impl Default for VendedoresJson {
	fn default() -> VendedoresJson {
		VendedoresJson{
			vendedores: Vec::new()
		}
	}
}


//-----------------------------------------------------------------------

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct LoginUserTemp {
	pub id_log: i32,
	pub usuario: String,
	pub clave: String,
	pub id_usu: i32,
	pub tipo: String,
	pub estado: String
}

impl Default for LoginUserTemp {
	fn default() -> LoginUserTemp {
		LoginUserTemp{
			id_log: 0,
			usuario:"".to_string(),
            clave:"".to_string(),
            id_usu: 0,
            tipo:"".to_string(),
            estado:"".to_string()
		}
	}
}

impl fmt::Display for LoginUserTemp {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"id_log:{}, usuario:{}, clave:{}, id_usu:{}, tipo:{}, estado:{}",
        self.id_log, self.usuario, self.clave, self.id_usu, self.tipo, self.estado)
    }
}