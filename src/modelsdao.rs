use mysql::{params,Row};

use rocket::{get,State};
use rocket_contrib::json::{Json,JsonValue};

use crate::models::*;
use crate::connect::*;

// ----- ADMIN USERS 

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct AdminUsers {
    //cnxpool: &MysqlPool,
    //source: Vec<AdminUser>
}

/* impl Default for AdminUsers {
	fn default() -> AdminUsers {
		AdminUsers{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl AdminUsers {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &AdminUser) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into admin_users_t (nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,tipo,
			cr_creditos,cr_creditos_usa,cr_fecha_mod,nivel)
		Values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}','{}')", any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
		any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),any.tipo.to_owned(),
        any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned(),any.nivel.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<AdminUser>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into admin_users_t (nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,tipo,
                cr_creditos,cr_creditos_usa,cr_fecha_mod,nivel)
            Values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}','{}')", item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),item.tipo.to_owned(),item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned(),item.nivel.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &AdminUser){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update admin_users_t set nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', tipo = '{}',
            cr_creditos = {}, cr_creditos_usa = {}, cr_fecha_mod = '{}', nivel = '{}' Where id_auser = {}", any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
        any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),any.tipo.to_owned(),any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned(),any.nivel.to_owned(),any.id_auser);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
    pub fn edit_nivel(cnxpool: &MysqlPool, crit: String, level: String){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update admin_users_t set nivel = '{}' WHERE {}", level, crit);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<AdminUser>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update admin_users_t set nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', tipo = '{}',
            cr_creditos = {}, cr_creditos_usa = {}, cr_fecha_mod = '{}', nivel = '{}' Where id_auser = {}", item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),item.tipo.to_owned(),item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned(),item.nivel.to_owned(),item.id_auser);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from admin_users_t Where id_auser = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<AdminUser> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,tipo,
        cr_creditos,cr_creditos_usa,cr_fecha_mod,nivel from admin_users_t Where {}", crit);

        println!("{}", sql);
        let adm_users: Vec<AdminUser> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        AdminUser{
                            id_auser: row.get(0).unwrap(),
                            nombre: row.get(1).unwrap(),
                            usuario: row.get(2).unwrap(),
                            clave: row.get(3).unwrap(),
                            fecha: row.get(4).unwrap(), 
                            fecha_login: row.get(5).unwrap(),
                            sesion: row.get(6).unwrap(),
                            sesion_tok: row.get(7).unwrap(),
                            pais: row.get(8).unwrap(),
                            estado: row.get(9).unwrap(),
                            tipo: row.get(10).unwrap(), 
                            cr_creditos: row.get(11).unwrap(),
                            cr_creditos_usa: row.get(12).unwrap(),
                            cr_fecha_mod: row.get(13).unwrap(),
                            nivel: row.get(14).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", adm_users);
        adm_users
    }
    pub fn filter_groups(cnxpool: &MysqlPool, crit: String) -> Vec<AdminUser> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("SELECT id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,tipo,
        cr_creditos,cr_creditos_usa,cr_fecha_mod,nivel, LOCATE('{}',nivel) FROM admin_users_t WHERE  tipo = 'GRUPO' AND  LOCATE('{}',nivel)=1  ;", crit, crit);

        println!("{}", sql);
        let adm_users: Vec<AdminUser> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        AdminUser{
                            id_auser: row.get(0).unwrap(),
                            nombre: row.get(1).unwrap(),
                            usuario: row.get(2).unwrap(),
                            clave: row.get(3).unwrap(),
                            fecha: row.get(4).unwrap(), 
                            fecha_login: row.get(5).unwrap(),
                            sesion: row.get(6).unwrap(),
                            sesion_tok: row.get(7).unwrap(),
                            pais: row.get(8).unwrap(),
                            estado: row.get(9).unwrap(),
                            tipo: row.get(10).unwrap(), 
                            cr_creditos: row.get(11).unwrap(),
                            cr_creditos_usa: row.get(12).unwrap(),
                            cr_fecha_mod: row.get(13).unwrap(),
                            nivel: row.get(14).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", adm_users);
        adm_users
    }
    pub fn filter_administrators(cnxpool: &MysqlPool, crit: String) -> Vec<AdminUser> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("SELECT id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,tipo,
        cr_creditos,cr_creditos_usa,cr_fecha_mod,nivel  FROM admin_users_t WHERE  tipo = 'ADMINISTRADOR'   {}", crit);

        println!("{}", sql);
        let adm_users: Vec<AdminUser> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        AdminUser{
                            id_auser: row.get(0).unwrap(),
                            nombre: row.get(1).unwrap(),
                            usuario: row.get(2).unwrap(),
                            clave: row.get(3).unwrap(),
                            fecha: row.get(4).unwrap(), 
                            fecha_login: row.get(5).unwrap(),
                            sesion: row.get(6).unwrap(),
                            sesion_tok: row.get(7).unwrap(),
                            pais: row.get(8).unwrap(),
                            estado: row.get(9).unwrap(),
                            tipo: row.get(10).unwrap(), 
                            cr_creditos: row.get(11).unwrap(),
                            cr_creditos_usa: row.get(12).unwrap(),
                            cr_fecha_mod: row.get(13).unwrap(),
                            nivel: row.get(14).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", adm_users);
        adm_users
    }

    pub fn filter_sucursales(cnxpool: &MysqlPool, crit: String) -> Vec<AdminUser> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("SELECT id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,tipo,
        cr_creditos,cr_creditos_usa,cr_fecha_mod,nivel  FROM admin_users_t WHERE  tipo = 'SUCURSAL'   {}", crit);

        println!("{}", sql);
        let adm_users: Vec<AdminUser> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        AdminUser{
                            id_auser: row.get(0).unwrap(),
                            nombre: row.get(1).unwrap(),
                            usuario: row.get(2).unwrap(),
                            clave: row.get(3).unwrap(),
                            fecha: row.get(4).unwrap(), 
                            fecha_login: row.get(5).unwrap(),
                            sesion: row.get(6).unwrap(),
                            sesion_tok: row.get(7).unwrap(),
                            pais: row.get(8).unwrap(),
                            estado: row.get(9).unwrap(),
                            tipo: row.get(10).unwrap(), 
                            cr_creditos: row.get(11).unwrap(),
                            cr_creditos_usa: row.get(12).unwrap(),
                            cr_fecha_mod: row.get(13).unwrap(),
                            nivel: row.get(14).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", adm_users);
        adm_users
    }

    pub fn json_to_struc(ausers: Json<AdminUsersJson>) -> Vec<AdminUser> {
        let mut adm_users : Vec<AdminUser> = Vec::new();
        for auser in ausers.adm_users.clone() {
            adm_users.push(auser);
        }
        adm_users
    }
    
    //RECIVE STRING NIVEL DEVUELVE ARRAY paso 1
    pub fn getLevelsToArr(level: &str) -> Vec<i32> {
        let levellen = level.len();
        let slice = &level[0..levellen];
        let nums: Vec<&str> = slice.split(",").collect();
        let mut levels: Vec<i32> = Vec::new();
        for num in nums{
            let vnum = num.parse::<i32>().unwrap();
            levels.push(vnum);
        }
        levels
    }


    //inyecta el id EN  EL NIVEL PASO 2
    pub fn getLevelId(levels: Vec<i32>, id_auser: i32) -> Vec<i32> {
        let mut band: i32 = 0;
        let mut levelsMod : Vec<i32> = Vec::new();
        let mut levelNew : i32 = 0;
        for level in levels {
            levelNew = level;
            if (band == 0 && level == 0) {
                levelNew = id_auser;
                band = 1;
            }
            levelsMod.push(levelNew);
        }
        levelsMod
    }
        // //recive un array nivel retorna string PASO 3
        // pub fn getLevelsToString(levels: Vec<i32>) -> String {
        //     let nums_str: String = levels.into_iter().map(|i| i.to_string()).collect::<String>();
        //     nums_str
        // }

        pub fn getLevelsToString(levels: Vec<i32>) -> String {
            let mut nums_str: String = "".to_string();
            for (i, num) in levels.iter().enumerate() {
                if(i != 3){
                    nums_str.push_str(&format!("{},", num.to_string()));
                }else{
                    nums_str.push_str(&format!("{}", num.to_string()));
                }
            }
            nums_str
        }
}

//---- CAPOST

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct CsApos {
    //cnxpool: &MysqlPool,
    //source: Vec<AdminUser>
}

/* impl Default for AdminUsers {
	fn default() -> AdminUsers {
		AdminUsers{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl CsApos {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &CApos) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into capos_temp_t (id_partida,carton,estado,id_ticket,id_usuario,id_pais,moneda,compat,tipo_usu)
		Values('{}','{}','{}','{}','{}','{}','{}','{}','{}')", any.id_partida,any.carton.to_owned(),any.estado.to_owned(),any.id_ticket,
		any.id_usuario,any.id_pais,any.moneda.to_owned(),any.compat.to_owned(),any.tipo_usu.to_owned());
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<CApos>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into capos_temp_t (id_partida,carton,estado,id_ticket,id_usuario,id_pais,moneda,compat,tipo_usu)
            Values('{}','{}','{}','{}','{}','{}','{}','{}','{}')", item.id_partida,item.carton.to_owned(),item.estado.to_owned(),item.id_ticket,
            item.id_usuario,item.id_pais,item.moneda.to_owned(),item.compat.to_owned(),item.tipo_usu.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &CApos){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update capos_temp_t set id_partida = {},carton = '{}',estado = '{}',id_ticket = {},id_usuario = {},id_pais = {},moneda = '{}',compat = '{}', tipo_usu = '{}' Where id_capos = {}",any.id_partida,any.carton.to_owned(),any.estado.to_owned(),any.id_ticket,
        any.id_usuario,any.id_pais,any.moneda.to_owned(),any.compat.to_owned(),any.tipo_usu.to_owned(),any.id_capos);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<CApos>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update capos_temp_t set id_partida = {},carton = '{}',estado = '{}',id_ticket = {},id_usuario = {},id_pais = {},moneda = '{}',compat = '{}', tipo_usu = '{}' Where id_capos = {}",item.id_partida,item.carton.to_owned(),item.estado.to_owned(),item.id_ticket,
            item.id_usuario,item.id_pais,item.moneda.to_owned(),item.compat.to_owned(),item.tipo_usu.to_owned(),item.id_capos);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from capos_temp_t Where id_ticket = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<CApos> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_capos,id_partida,carton,estado,id_ticket,id_usuario,id_pais,moneda,compat,tipo_usu from capos_temp_t Where {}", crit);

        println!("{}", sql);
        let csapos: Vec<CApos> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        CApos{
                            id_capos: row.get(0).unwrap(),
                            id_partida: row.get(1).unwrap(),
                            carton: row.get(2).unwrap(),
                            estado: row.get(3).unwrap(),
                            id_ticket: row.get(4).unwrap(),
                            id_usuario: row.get(5).unwrap(),
                            id_pais: row.get(6).unwrap(),
                            moneda: row.get(7).unwrap(),
                            compat: row.get(8).unwrap(),
                            tipo_usu: row.get(9).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", csapos);
        csapos
    }
    pub fn json_to_struc(csapos: Json<CsAposJson>) -> Vec<CApos> {
        let mut csapost : Vec<CApos> = Vec::new();
        for capos in csapos.csapos.clone() {
            csapost.push(capos);
        }
        csapost
	}
}


// ----- CARTONES

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Cartones {
    //cnxpool: &MysqlPool,
    //source: Vec<Carton>
}

/* impl Default for Cartones {
	fn default() -> Cartones {
		Cartones{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Cartones {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Carton) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into cartones_t (carton) Values('{}')", any.carton.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Carton>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into cartones_t (carton) Values('{}')", item.carton.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Carton){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update cartones_t set cartones = '{}' Where id_cart = {}", any.carton.to_owned(),any.id_cart);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Carton>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update cartones_t set carton = '{}' Where id_cart = {}", item.carton.to_owned(),item.id_cart);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from cartones_t Where id_cart = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Carton> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_cart,carton from cartones_t Where {}", crit);

        println!("{}", sql);
        let cartones: Vec<Carton> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Carton{
                            id_cart: row.get(0).unwrap(),
                            carton: row.get(1).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", cartones);
        cartones
    }
    pub fn json_to_struc(cartonesJ: Json<CartonesJson>) -> Vec<Carton> {
        let mut cartones : Vec<Carton> = Vec::new();
        for carton in cartonesJ.cartones.clone() {
            cartones.push(carton);
        }
        cartones
	}
}


// ----- GANANCIAS

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Ganancias {
    //cnxpool: &MysqlPool,
    //source: Vec<Ganancia>
}

/* impl Default for Ganancias {
	fn default() -> Ganancias {
		Ganancias{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Ganancias {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Ganancia) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into ganancias_t (id_usuario, id_padre, porcentaje, porcentaje_padre, fecha, fecha_mod) Values('{}','{}','{}','{}','{}','{}')", any.id_usuario, any.id_padre, any.porcentaje, any.porcentaje_padre, any.fecha.to_owned(), any.fecha_mod.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Ganancia>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into ganancias_t (id_usuario, id_padre, porcentaje, porcentaje_padre, fecha, fecha_mod) Values({},{},{},{},'{}','{}')", item.id_usuario, item.id_padre, item.porcentaje, item.porcentaje_padre, item.fecha.to_owned(), item.fecha_mod.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Ganancia){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update ganancias_t set id_usuario = {}, id_padre = {}, porcentaje = {}, porcentaje_padre = {}, fecha = '{}', fecha_mod = '{}' Where id_gan = {}", any.id_usuario, any.id_padre, any.porcentaje, any.porcentaje_padre, any.fecha.to_owned(), any.fecha_mod.to_owned(), any.id_gan);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Ganancia>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update ganancias_t set id_usuario = {}, id_padre = {}, porcentaje = {}, porcentaje_padre = {}, fecha = '{}', fecha_mod = '{}' Where id_gan = {}", item.id_usuario, item.id_padre, item.porcentaje, item.porcentaje_padre, item.fecha.to_owned(), item.fecha_mod.to_owned(), item.id_gan);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from ganancias_t Where id_gan = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Ganancia> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_gan, id_usuario, id_padre, porcentaje, porcentaje_padre, fecha, fecha_mod from ganancias_t Where {}", crit);

        println!("{}", sql);
        let ganancias: Vec<Ganancia> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Ganancia{
                            id_gan: row.get(0).unwrap(),
                            id_usuario: row.get(1).unwrap(), 
                            id_padre: row.get(2).unwrap(), 
                            porcentaje: row.get(3).unwrap(), 
                            porcentaje_padre: row.get(4).unwrap(), 
                            fecha: row.get(5).unwrap(), 
                            fecha_mod: row.get(6).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", ganancias);
        ganancias
    }
    pub fn json_to_struc(gananciasJ: Json<GananciasJson>) -> Vec<Ganancia> {
        let mut ganancias : Vec<Ganancia> = Vec::new();
        for ganancia in gananciasJ.ganancias.clone() {
            ganancias.push(ganancia);
        }
        ganancias
	}
}


// ----- JACKPOTS

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Jackpots {
    //cnxpool: &MysqlPool,
    //source: Vec<Jackpot>
}

/* impl Default for Jackpots {
	fn default() -> Jackpots {
		Jackpots{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Jackpots {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Jackpot) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into jackpots_t (tipo,fecha_creado,fecha_cobro,porcentaje,sorteos_acumulados,contribucion,ganadores,total,idpais) Values('{}','{}','{}',{},{},{},{},{},{})", any.tipo.to_owned(),any.fecha_cobro.to_owned(),any.fecha_creado.to_owned(),any.porcentaje,any.sorteos_acumulados,any.contribucion,any.ganadores,any.total,any.idpais);
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Jackpot>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into jackpots_t (tipo,fecha_creado,fecha_cobro,porcentaje,sorteos_acumulados,contribucion,ganadores,total,idpais) Values('{}','{}','{}',{},{},{},{},{},{})", item.tipo.to_owned(),item.fecha_cobro.to_owned(),item.fecha_creado.to_owned(),item.porcentaje,item.sorteos_acumulados,item.contribucion,item.ganadores,item.total,item.idpais);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Jackpot){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update jackpots_t set tipo = '{}',fecha_creado = '{}',fecha_cobro = '{}',porcentaje = {},sorteos_acumulados = {},contribucion = {},ganadores = '{}',total = {},idpais = {} Where id_jack = {}", any.tipo.to_owned(),any.fecha_cobro.to_owned(),any.fecha_creado.to_owned(),any.porcentaje,any.sorteos_acumulados,any.contribucion,any.ganadores,any.total,any.idpais,any.id_jack);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Jackpot>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update jackpots_t set tipo = '{}',fecha_creado = '{}',fecha_cobro = '{}',porcentaje = {},sorteos_acumulados = {},contribucion = {},ganadores = '{}',total = {},idpais = {} Where id_jack = {}", item.tipo.to_owned(),item.fecha_cobro.to_owned(),item.fecha_creado.to_owned(),item.porcentaje,item.sorteos_acumulados,item.contribucion,item.ganadores,item.total,item.idpais,item.id_jack);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from jackpots_t Where id_jack = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Jackpot> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_jack,tipo,fecha_creado,fecha_cobro,porcentaje,sorteos_acumulados,contribucion,ganadores,total,idpais from jackpots_t Where {}", crit);

        println!("{}", sql);
        let jackpots: Vec<Jackpot> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Jackpot{
                            id_jack: row.get(0).unwrap(),
                            tipo: row.get(1).unwrap(),
                            fecha_creado: row.get(2).unwrap(),
                            fecha_cobro: row.get(3).unwrap(),
                            porcentaje: row.get(4).unwrap(),
                            sorteos_acumulados: row.get(5).unwrap(),
                            contribucion: row.get(6).unwrap(),
                            ganadores: row.get(7).unwrap(),
                            total: row.get(8).unwrap(),
                            idpais: row.get(9).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", jackpots);
        jackpots
    }
    pub fn json_to_struc(jackpotsJ: Json<JackpotsJson>) -> Vec<Jackpot> {
        let mut jackpots : Vec<Jackpot> = Vec::new();
        for jackpot in jackpotsJ.jackpots.clone() {
            jackpots.push(jackpot);
        }
        jackpots
	}
}

// ----- PARTIDAS

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Partidas {
    //cnxpool: &MysqlPool,
    //source: Vec<Partida>
}

/* impl Default for Partidas {
	fn default() -> Partidas {
		Partidas{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Partidas {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Partida) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into partida_t (fecha,estado,tipo,bolitas_sorteo) Values('{}','{}','{}','{}')", any.fecha.to_owned(),any.estado.to_owned(),any.tipo.to_owned(),any.bolitas_sorteo.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Partida>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into partida_t (fecha,estado,tipo,bolitas_sorteo) Values('{}','{}','{}','{}')", item.fecha.to_owned(),item.estado.to_owned(),item.tipo.to_owned(),item.bolitas_sorteo.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Partida){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update partida_t set fecha = '{}', estado = '{}', tipo = '{}', bolitas_sorteo = '{}' Where id_part = {}", any.fecha.to_owned(),any.estado.to_owned(),any.tipo.to_owned(),any.bolitas_sorteo.to_owned(),any.id_part);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Partida>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update partida_t set fecha = '{}', estado = '{}', tipo = '{}', bolitas_sorteo = '{}' Where id_part = {}", item.fecha.to_owned(),item.estado.to_owned(),item.tipo.to_owned(),item.bolitas_sorteo.to_owned(),item.id_part);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from partida_t Where id_part = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Partida> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_part,fecha,estado,tipo,bolitas_sorteo from partida_t Where {}", crit);

        println!("{}", sql);
        let partidas: Vec<Partida> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Partida{
                            id_part: row.get(0).unwrap(),
                            fecha: row.get(1).unwrap(),
                            estado: row.get(2).unwrap(),
                            tipo: row.get(3).unwrap(),
                            bolitas_sorteo: row.get(4).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", partidas);
        partidas
    }
    pub fn json_to_struc(partidasJ: Json<PartidasJson>) -> Vec<Partida> {
        let mut partidas : Vec<Partida> = Vec::new();
        for partida in partidasJ.partidas.clone() {
            partidas.push(partida);
        }
        partidas
	}
}

// ----- SUCURSALES

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Sucursales {
    //cnxpool: &MysqlPool,
    //source: Vec<Sucursal>
}

/* impl Default for Sucursales {
	fn default() -> Sucursales {
		Sucursales{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Sucursales {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Sucursal) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into sucursal_t (id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,direccion,provincia,
			cr_creditos,cr_creditos_usa,cr_fecha_mod)
		Values({},'{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}')", any.id_auser,any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
		any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),any.direccion.to_owned(),any.provincia.to_owned(),
        any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Sucursal>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into sucursal_t (id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,direccion,provincia,
                cr_creditos,cr_creditos_usa,cr_fecha_mod)
            Values({},'{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}')", item.id_auser,item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),item.direccion.to_owned(),item.provincia.to_owned(),
            item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Sucursal){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update sucursal_t set id_auser = '{}', nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', direccion = '{}', provincia = '{}', 
        cr_creditos = '{}', cr_creditos_usa = '{}', cr_fecha_mod = '{}' Where id_suc = {}", any.id_auser,any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
        any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),any.direccion.to_owned(),any.provincia.to_owned(),any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned(),any.id_suc);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Sucursal>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update sucursal_t set id_auser = '{}', nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', direccion = '{}', provincia = '{}', 
            cr_creditos = '{}', cr_creditos_usa = '{}', cr_fecha_mod = '{}' Where id_suc = {}", item.id_auser,item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),item.direccion.to_owned(),item.provincia.to_owned(),item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned(),item.id_suc);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from sucursal_t Where id_suc = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Sucursal> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_suc,id_auser,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,direccion,provincia,
        cr_creditos,cr_creditos_usa,cr_fecha_mod from sucursal_t  {}", crit);

        println!("{}", sql);
        let sucursales: Vec<Sucursal> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Sucursal{
                            id_suc: row.get(0).unwrap(),
                            id_auser: row.get(1).unwrap(),
                            nombre: row.get(2).unwrap(),
                            usuario: row.get(3).unwrap(),
                            clave: row.get(4).unwrap(),
                            fecha: row.get(5).unwrap(),
                            fecha_login: row.get(6).unwrap(),
                            sesion: row.get(7).unwrap(),
                            sesion_tok: row.get(8).unwrap(),
                            pais: row.get(9).unwrap(),
                            estado: row.get(10).unwrap(),
                            direccion: row.get(11).unwrap(),
                            provincia: row.get(12).unwrap(),
                            cr_creditos: row.get(13).unwrap(),
                            cr_creditos_usa: row.get(14).unwrap(),
                            cr_fecha_mod: row.get(15).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", sucursales);
        sucursales
    }
    pub fn json_to_struc(sucursalesJ: Json<SucursalesJson>) -> Vec<Sucursal> {
        let mut sucursales : Vec<Sucursal> = Vec::new();
        for sucursal in sucursalesJ.sucursales.clone() {
            sucursales.push(sucursal);
        }
        sucursales
	}
}

// ----- TERMINALES

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Terminales {
    //cnxpool: &MysqlPool,
    //source: Vec<Terminal>
}

/* impl Default for Terminales {
	fn default() -> Terminales {
		Terminales{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Terminales {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Terminal) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into terminal_t (id_suc,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,
			cr_creditos,cr_creditos_usa,cr_fecha_mod)
		Values({},'{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}')", any.id_suc,any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
		any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),
        any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> {println!("{}", e); id = 0;}
		}
        id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Terminal>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into terminal_t (id_suc,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,
                cr_creditos,cr_creditos_usa,cr_fecha_mod)
            Values({},'{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}')", item.id_suc,item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),
            item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(e)=> { println!("{}", e); id = 0}
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Terminal){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update terminal_t set id_suc = '{}', nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', 
        cr_creditos = '{}', cr_creditos_usa = '{}', cr_fecha_mod = '{}' Where id_ter = {}", any.id_suc,any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
        any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned(),any.id_ter);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Terminal>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update terminal_t set id_suc = '{}', nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', 
            cr_creditos = '{}', cr_creditos_usa = '{}', cr_fecha_mod = '{}' Where id_ter = {}", item.id_suc,item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned(),item.id_suc);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from terminal_t Where id_ter = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Terminal> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_ter,id_suc,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,
        cr_creditos,cr_creditos_usa,cr_fecha_mod from terminal_t  {}", crit);

        println!("{}", sql);
        let terminales: Vec<Terminal> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Terminal{
                            id_ter: row.get(0).unwrap(),
                            id_suc: row.get(1).unwrap(),
                            nombre: row.get(2).unwrap(),
                            usuario: row.get(3).unwrap(),
                            clave: row.get(4).unwrap(),
                            fecha: row.get(5).unwrap(),
                            fecha_login: row.get(6).unwrap(),
                            sesion: row.get(7).unwrap(),
                            sesion_tok: row.get(8).unwrap(),
                            pais: row.get(9).unwrap(),
                            estado: row.get(10).unwrap(),
                            cr_creditos: row.get(11).unwrap(),
                            cr_creditos_usa: row.get(12).unwrap(),
                            cr_fecha_mod: row.get(13).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", terminales);
        terminales
    }
    pub fn json_to_struc(terminalesJ: Json<TerminalesJson>) -> Vec<Terminal> {
        let mut terminales : Vec<Terminal> = Vec::new();
        for terminal in terminalesJ.terminales.clone() {
            terminales.push(terminal);
        }
        terminales
	}
}


// ----- TICKETS

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Tickets {
    //cnxpool: &MysqlPool,
    //source: Vec<Ticket>
}

/* impl Default for Tickets {
	fn default() -> Tickets {
		Tickets{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Tickets {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Ticket) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Insert Into ticket_t (modalidad, cantidad_apostada, ganancia, pin, estado, ticket_sucursal, id_sucursal, id_carton, carton, id_partida, id_usuario, tipo_bingo, moneda, id_pais, compat, fecha_emision, fecha_cobro, estfecha, estobs, pagado, tipo_usu) 
        Values('{}',{},{},{},'{}',{},{},{},'{}',{},{},'{}','{}',{},'{}','{}','{}','{}','{}',{},'{}')", any.modalidad.to_owned(), any.cantidad_apostada, any.ganancia, any.pin, any.estado.to_owned(), any.ticket_sucursal, any.id_sucursal, 
        any.id_carton, any.carton.to_owned(), any.id_partida, any.id_usuario, any.tipo_bingo.to_owned(), any.moneda.to_owned(), any.id_pais, any.compat.to_owned(), any.fecha_emision.to_owned(), any.fecha_cobro.to_owned(), any.estfecha.to_owned(), any.estobs.to_owned(), any.pagado, any.tipo_usu.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Ticket>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into ticket_t (modalidad, cantidad_apostada, ganancia, pin, estado, ticket_sucursal, id_sucursal, id_carton, carton, id_partida, id_usuario, tipo_bingo, moneda, id_pais, compat, fecha_emision, fecha_cobro, estfecha, estobs, pagado, tipo_usu) 
            Values('{}',{},{},{},'{}',{},{},{},'{}',{},{},'{}','{}',{},'{}','{}','{}','{}','{}',{},'{}')", item.modalidad.to_owned(), item.cantidad_apostada, item.ganancia, item.pin, item.estado.to_owned(), item.ticket_sucursal, item.id_sucursal, 
            item.id_carton, item.carton.to_owned(), item.id_partida, item.id_usuario, item.tipo_bingo.to_owned(), item.moneda.to_owned(), item.id_pais, item.compat.to_owned(), item.fecha_emision.to_owned(), item.fecha_cobro.to_owned(), item.estfecha.to_owned(), item.estobs.to_owned(), item.pagado, item.tipo_usu.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Ticket){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update ticket_t set modalidad = '{}' , cantidad_apostada = '{}' , ganancia = '{}' , pin = '{}' , estado = '{}' , ticket_sucursal = '{}' , id_sucursal = '{}' , id_carton = '{}' , carton = '{}' , id_partida = '{}' , id_usuario = '{}' , tipo_bingo = '{}' , moneda = '{}' , id_pais = '{}' , compat = '{}' , fecha_emision = '{}' , fecha_cobro = '{}' , estfecha = '{}' , estobs = '{}' , pagado = '{}', tipo_usu = '{}' Where id_tick = {}", 
        any.modalidad.to_owned(), any.cantidad_apostada, any.ganancia, any.pin, any.estado.to_owned(), any.ticket_sucursal, any.id_sucursal, any.id_carton, any.carton.to_owned(), any.id_partida, any.id_usuario, any.tipo_bingo.to_owned(), any.moneda.to_owned(), any.id_pais, any.compat.to_owned(), any.fecha_emision.to_owned(), any.fecha_cobro.to_owned(), any.estfecha.to_owned(), any.estobs.to_owned(), any.pagado, any.id_ticket, any.tipo_usu.to_owned());
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Ticket>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update ticket_t set modalidad = '{}' , cantidad_apostada = '{}' , ganancia = '{}' , pin = '{}' , estado = '{}' , ticket_sucursal = '{}' , id_sucursal = '{}' , id_carton = '{}' , carton = '{}' , id_partida = '{}' , id_usuario = '{}' , tipo_bingo = '{}' , moneda = '{}' , id_pais = '{}' , compat = '{}' , fecha_emision = '{}' , fecha_cobro = '{}' , estfecha = '{}' , estobs = '{}' , pagado = '{}', tipo_usu = '{}' Where id_ticket = {}", 
            item.modalidad.to_owned(), item.cantidad_apostada, item.ganancia, item.pin, item.estado.to_owned(), item.ticket_sucursal, item.id_sucursal, item.id_carton, item.carton.to_owned(), item.id_partida, item.id_usuario, item.tipo_bingo.to_owned(), item.moneda.to_owned(), item.id_pais, item.compat.to_owned(), item.fecha_emision.to_owned(), item.fecha_cobro.to_owned(), item.estfecha.to_owned(), item.estobs.to_owned(), item.pagado, item.id_ticket, item.tipo_usu.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from ticket_t Where id_ticket = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Ticket> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_ticket, modalidad, cantidad_apostada, ganancia, pin, estado, ticket_sucursal, id_sucursal, id_carton, carton, id_partida, id_usuario, tipo_bingo, moneda, id_pais, compat, fecha_emision, fecha_cobro, estfecha, estobs, pagado, tipo_usu from ticket_t Where {}", crit);

        println!("{}", sql);
        let tickets: Vec<Ticket> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Ticket{
                            id_ticket: row.get(0).unwrap(),
                            modalidad: row.get(1).unwrap(),
                            cantidad_apostada: row.get(2).unwrap(),
                            ganancia: row.get(3).unwrap(),
                            pin: row.get(4).unwrap(),
                            estado: row.get(5).unwrap(),
                            ticket_sucursal: row.get(6).unwrap(),
                            id_sucursal: row.get(7).unwrap(),
                            id_carton: row.get(8).unwrap(),
                            carton: row.get(9).unwrap(),
                            id_partida: row.get(10).unwrap(),
                            id_usuario: row.get(11).unwrap(),
                            tipo_bingo: row.get(12).unwrap(),
                            moneda: row.get(13).unwrap(),
                            id_pais: row.get(14).unwrap(),
                            compat: row.get(15).unwrap(),
                            fecha_emision: row.get(16).unwrap(),
                            fecha_cobro: row.get(17).unwrap(),
                            estfecha: row.get(18).unwrap(),
                            estobs: row.get(19).unwrap(),
                            pagado: row.get(20).unwrap(),
                            tipo_usu: row.get(21).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", tickets);
        tickets
    }
    pub fn json_to_struc(ticketsJ: Json<TicketsJson>) -> Vec<Ticket> {
        let mut tickets : Vec<Ticket> = Vec::new();
        for ticket in ticketsJ.tickets.clone() {
            tickets.push(ticket);
        }
        tickets
    }

    pub fn filter_ticketimp(cnxpool: &MysqlPool, id_usu: i32, tipo_usu: String, anid: i32) -> Vec<TicketImp>{
        let mut pool = cnxpool.get().unwrap();
        let sqlTicket = format!("Select t.id_ticket, t.pin, t.ticket_sucursal, t.id_carton, t.carton, suc.nombre as sucursal, u.nombre as terminal, t.id_partida, p.simbolo, t.cantidad_apostada, t.fecha_emision
                                from ticket_t t
                                INNER join sucursal_t suc
                                on suc.id_suc = t.id_sucursal
                                INNER JOIN (
                                    SELECT id_ven as id_usu, id_suc, nombre, 'VENDEDOR' as tipo FROM vendedor_t
                                    union
                                    SELECT id_ter as id_usu, id_suc, nombre, 'TERMINAL' as tipo FROM terminal_t
                                ) u
                                ON u.id_usu = {} And u.tipo = '{}' AND u.id_suc = suc.id_suc
                                INNER JOIN paises_moneda_t p
                                ON p.id_pais = t.id_pais
                                WHERE t.id_ticket = {}", id_usu, tipo_usu, anid);
        let tickets: Vec<TicketImp> = pool.prep_exec(sqlTicket,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        TicketImp{
                            id_ticket: row.get(0).unwrap(), 
                            ticket0: "".to_string(),
                            pin: row.get(1).unwrap(), 
                            code: "".to_string(),
                            ticket_sucursal: row.get(2).unwrap(), 
                            id_carton: row.get(3).unwrap(), 
                            carton: row.get(4).unwrap(),
                            sucursal: row.get(5).unwrap(),
                            terminal: row.get(6).unwrap(),
                            id_partida: row.get(7).unwrap(),
                            simbolo: row.get(8).unwrap(),
                            monto: row.get(9).unwrap(),
                            fecha_emision: row.get(10).unwrap()
                        }
                }).collect()
        }).unwrap();
        tickets
    }

    pub fn getBarcode(ticketImp: TicketImp) -> TicketImp{

        let pinN: String = ticketImp.pin.to_string();
        let pinArr: Vec<char> = pinN.chars().collect();
    
        let idticketN: String = ticketImp.id_ticket.to_string();
        let ticketArrAux: Vec<char> = idticketN.chars().collect();
        let mut barcode = "".to_string();
        let mut pinInd = 0;
        let mut ticketInd = 0;
        let mut ticket0 = "".to_string();
    
        let mut ticket: [i32;7] = [0;7];
    
        for i in 0..(7 - ticketArrAux.len()) {
            ticket[i] = 0;
            ticket0.push_str(&"0".to_string());
            println!("{:?}, {:?}, {}", ticket, ticket0, ticketArrAux.len());
        }
    
        println!("{:?} {:?} {:?}", ticket, pinArr, ticketArrAux);
        let length = (7 - ticketArrAux.len());
    
        for i in length..7 {
            let n: i32 = ticketArrAux[i - length].to_digit(10).unwrap() as i32;
            ticket[i] =  n;
            ticket0.push_str(&ticketArrAux[i - length].to_string());
    
            println!("AS {:?}, {:?}", ticket, ticket0);
        }
        
        for i in 0..12 {
            if(i == 1 || i == 2 || i == 3 || i == 5 || i == 6 || i == 7 || i == 9){
                barcode.push_str(&ticket[ticketInd].to_string());
                ticketInd += 1;
            }else{
                barcode.push_str(&pinArr[pinInd].to_string());
                pinInd += 1;
            }
        }
        
        println!("{:?}", barcode);
    
        TicketImp{
            id_ticket: ticketImp.id_ticket,
            ticket0: ticket0, 
            pin: ticketImp.pin,
            code: barcode,
            ticket_sucursal: ticketImp.ticket_sucursal,
            id_carton: ticketImp.id_carton,
            carton: ticketImp.carton,
            sucursal: ticketImp.sucursal,
            terminal: ticketImp.terminal,
            id_partida: ticketImp.id_partida,
            simbolo: ticketImp.simbolo,
            monto: ticketImp.monto,
            fecha_emision: ticketImp.fecha_emision

        }
    }
}


// ----- VENDEDORES

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Vendedores {
    //cnxpool: &MysqlPool,
    //source: Vec<Vendedor>
}

/* impl Default for Vendedores {
	fn default() -> Vendedores {
		Vendedores{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Vendedores {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Vendedor) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into vendedor_t (id_suc,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,
			cr_creditos,cr_creditos_usa,cr_fecha_mod)
		Values({},'{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}')", any.id_suc,any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
		any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),
        any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned());
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Vendedor>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into vendedor_t (id_suc,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,direccion,provincia,
                cr_creditos,cr_creditos_usa,cr_fecha_mod)
            Values({},'{}','{}','{}','{}','{}','{}','{}','{}','{}',{},{},'{}')", item.id_suc,item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),
            item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned());
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Vendedor){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update vendedor_t set id_suc = '{}', nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', 
        cr_creditos = '{}', cr_creditos_usa = '{}', cr_fecha_mod = '{}' Where id_ven = {}", any.id_suc,any.nombre.to_owned(),any.usuario.to_owned(),any.clave.to_owned(),any.fecha.to_owned(),
        any.fecha_login.to_owned(),any.sesion.to_owned(),any.sesion_tok.to_owned(),any.pais.to_owned(),any.estado.to_owned(),any.cr_creditos,any.cr_creditos_usa,any.cr_fecha_mod.to_owned(),any.id_suc);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Vendedor>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update vendedor_t set id_suc = '{}', nombre = '{}', usuario = '{}', clave = '{}', fecha = '{}', fecha_login = '{}', sesion = '{}', sesion_tok = '{}', pais = '{}', estado = '{}', 
            cr_creditos = '{}', cr_creditos_usa = '{}', cr_fecha_mod = '{}' Where id_ven = {}", item.id_suc,item.nombre.to_owned(),item.usuario.to_owned(),item.clave.to_owned(),item.fecha.to_owned(),
            item.fecha_login.to_owned(),item.sesion.to_owned(),item.sesion_tok.to_owned(),item.pais.to_owned(),item.estado.to_owned(),item.cr_creditos,item.cr_creditos_usa,item.cr_fecha_mod.to_owned(),item.id_suc);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from vendedor_t Where id_ven = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Vendedor> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_ven,id_suc,nombre,usuario,clave,fecha,fecha_login,sesion,sesion_tok,pais,estado,
        cr_creditos,cr_creditos_usa,cr_fecha_mod from vendedor_t Where {}", crit);

        println!("{}", sql);
        let vendedores: Vec<Vendedor> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Vendedor{
                            id_ven: row.get(0).unwrap(),
                            id_suc: row.get(1).unwrap(),
                            nombre: row.get(2).unwrap(),
                            usuario: row.get(3).unwrap(),
                            clave: row.get(4).unwrap(),
                            fecha: row.get(5).unwrap(),
                            fecha_login: row.get(6).unwrap(),
                            sesion: row.get(7).unwrap(),
                            sesion_tok: row.get(8).unwrap(),
                            pais: row.get(9).unwrap(),
                            estado: row.get(10).unwrap(),
                            cr_creditos: row.get(11).unwrap(),
                            cr_creditos_usa: row.get(12).unwrap(),
                            cr_fecha_mod: row.get(13).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", vendedores);
        vendedores
    }
    pub fn json_to_struc(vendedoresJ: Json<VendedoresJson>) -> Vec<Vendedor> {
        let mut vendedores : Vec<Vendedor> = Vec::new();
        for vendedor in vendedoresJ.vendedores.clone() {
            vendedores.push(vendedor);
        }
        vendedores
	}
}


// ----- PAISES

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Paises {
    //cnxpool: &MysqlPool,
    //source: Vec<Carton>
}

/* impl Default for Paises {
	fn default() -> Paises {
		Paises{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl Paises {
	/* pub fn setpool(self, pool: &MysqlPool){
		self.cnxpool = pool;
	} */
	pub fn create(cnxpool: &MysqlPool, any: &Pais) -> u64 {
		let mut cnx = cnxpool.get().unwrap();
		let sql = format!("Insert Into paises_moneda_t (nombre, codigo, simbolo, valor_carton, fijo_pozo, fijo_jackpot, porcentaje_pozo,
            porcentaje_jackpot, porcentaje_cliente, porcentaje_vendedor) Values('{}','{}','{}',{},{},{},{},{},{},{})", any.nombre.to_owned(),
            any.codigo.to_owned(),any.simbolo.to_owned(),any.valor_carton,any.fijo_pozo,any.fijo_jackpot,any.porcentaje_pozo,any.porcentaje_jackpot,
            any.porcentaje_cliente,any.porcentaje_vendedor);
        println!("{}", sql);
		let mut stm = cnx.prepare(sql).unwrap();
		let res = stm.execute(());
		let mut id = 0;
		match res {
			Ok(v) => id = v.last_insert_id(),
			Err(e)=> id = 0
		}
		id
	}
	pub fn create_batch(cnxpool: &MysqlPool, items: &Vec<Pais>)  {
        let mut cnx = cnxpool.get().unwrap();
        let mut items_ins: Vec<u64> = Vec::new();
		for item in items{
            let sql = format!("Insert Into paises_moneda_t (nombre, codigo, simbolo, valor_carton, fijo_pozo, fijo_jackpot, porcentaje_pozo,
                porcentaje_jackpot, porcentaje_cliente, porcentaje_vendedor) Values('{}','{}','{}',{},{},{},{},{},{},{})", item.nombre.to_owned(),
                item.codigo.to_owned(),item.simbolo.to_owned(),item.valor_carton,item.fijo_pozo,item.fijo_jackpot,item.porcentaje_pozo,item.porcentaje_jackpot,
                item.porcentaje_cliente,item.porcentaje_vendedor);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
            let mut id = 0;
            match res {
                Ok(v) => id = v.last_insert_id(),
                Err(_)=> id = 0
            }
            items_ins.push(id);
        }
    }    
	pub fn edit(cnxpool: &MysqlPool, any: &Pais){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Update paises_moneda_t set nombre = '{}', codigo = '{}', simbolo = '{}', valor_carton = {}, fijo_pozo = {}, fijo_jackpot = {}, porcentaje_pozo = {},
        porcentaje_jackpot = {}, porcentaje_cliente = {}, porcentaje_vendedor = {} Where id_pais = {}", any.nombre.to_owned(),
        any.codigo.to_owned(),any.simbolo.to_owned(),any.valor_carton,any.fijo_pozo,any.fijo_jackpot,any.porcentaje_pozo,any.porcentaje_jackpot,
        any.porcentaje_cliente,any.porcentaje_vendedor,any.id_pais);
        println!("{}", sql);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn edit_batch(cnxpool: &MysqlPool, items: &Vec<Pais>){
        let mut cnx = cnxpool.get().unwrap();
		for item in items{
            let sql = format!("Update paises_moneda_t set nombre = {}, codigo = {}, simbolo = {}, valor_carton = {}, fijo_pozo = {}, fijo_jackpot = {}, porcentaje_pozo = {},
            porcentaje_jackpot = {}, porcentaje_cliente = {}, porcentaje_vendedor = '{}' Where id_pais = {}", item.nombre.to_owned(),
            item.codigo.to_owned(),item.simbolo.to_owned(),item.valor_carton,item.fijo_pozo,item.fijo_jackpot,item.porcentaje_pozo,item.porcentaje_jackpot,
            item.porcentaje_cliente,item.porcentaje_vendedor,item.id_pais);
            let mut stm = cnx.prepare(sql).unwrap();
            let res = stm.execute(());
        }
    }
	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from paises_moneda_t Where id_pais = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    }
	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<Pais> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_pais,nombre, codigo, simbolo, valor_carton, fijo_pozo, fijo_jackpot, porcentaje_pozo,
        porcentaje_jackpot, porcentaje_cliente, porcentaje_vendedor from paises_moneda_t  {}", crit);

        println!("{}", sql);
        let paises: Vec<Pais> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        Pais{
                            id_pais: row.get(0).unwrap(),
                            nombre: row.get(1).unwrap(), 
                            codigo: row.get(2).unwrap(),
                            simbolo: row.get(3).unwrap(),
                            valor_carton: row.get(4).unwrap(),
                            fijo_pozo: row.get(5).unwrap(),
                            fijo_jackpot: row.get(6).unwrap(), 
                            porcentaje_pozo: row.get(7).unwrap(),
                            porcentaje_jackpot: row.get(8).unwrap(), 
                            porcentaje_cliente: row.get(9).unwrap(), 
                            porcentaje_vendedor: row.get(10).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", paises);
        paises
    }
    pub fn json_to_struc(paisesJ: Json<PaisesJson>) -> Vec<Pais> {
        let mut paises : Vec<Pais> = Vec::new();
        for pais in paisesJ.paises.clone() {
            paises.push(pais);
        }
        paises
	}
}

// ----- LOGIN USER TEMP

#[allow(non_snake_case)]
#[derive(Debug)]
pub struct LoginUsersTemp {
    //cnxpool: &MysqlPool,
    //source: Vec<Carton>
}

/* impl Default for LoginUsersTemp {
	fn default() -> LoginUsersTemp {
		LoginUsersTemp{
            cnxpool: Ghook::getPool(),
			//source : Vec::new()
		}
	}
} */

impl LoginUsersTemp {

/* 	pub fn kill(cnxpool: &MysqlPool, anid: i32){
        let mut cnx = cnxpool.get().unwrap();
        let sql = format!("Delete from paises_moneda_t Where id_pais = {}", anid);
        let mut stm = cnx.prepare(sql).unwrap();
        let res = stm.execute(());
    } */

	pub fn filter(cnxpool: &MysqlPool, crit: String) -> Vec<LoginUserTemp> {
        let mut pool = cnxpool.get().unwrap();
        let sql = format!("Select id_log,usuario,clave,id_usu,tipo,estado from login_temp_t Where {}", crit);

        println!("{}", sql);
        let users_credentials: Vec<LoginUserTemp> = pool.prep_exec(sql,()).map(|result|{
                result.map(|x| x.unwrap()).map(|row|{
                        LoginUserTemp{
                            id_log: row.get(0).unwrap(),
                            usuario: row.get(1).unwrap(),
                            clave: row.get(2).unwrap(),
                            id_usu: row.get(3).unwrap(),
                            tipo: row.get(4).unwrap(),
                            estado: row.get(5).unwrap()
                        }
                }).collect()
        }).unwrap();
        println!("{:?}", users_credentials);
        users_credentials
    }

/*     pub fn json_to_struc(users_credentialsJ: Json<LoginUsersTempJson>) -> Vec<LoginUserTemp> {
        let mut users_credentials : Vec<LoginUserTemp> = Vec::new();
        for user_credential in users_credentialsJ.users_credentials.clone() {
            users_credentials.push(user_credential);
        }
        users_credentials
	} */
}