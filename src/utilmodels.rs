use std::fmt;

#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct LoginUser {
	pub usuario: String,
    pub clave: String
}

impl Default for LoginUser {
	fn default() -> LoginUser {
		LoginUser{
            usuario: "".to_string(),
            clave: "".to_string()
		}
	}
}


#[allow(non_snake_case)]
#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct WhereStr {
	pub wherestr: String
}

impl Default for WhereStr {
	fn default() -> WhereStr {
		WhereStr{
            wherestr: "".to_string()
		}
	}
}


